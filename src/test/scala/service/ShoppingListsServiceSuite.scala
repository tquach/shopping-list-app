package service

import org.scalatest.{ FunSuite, BeforeAndAfter }

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import reactivemongo.api._

import scala.concurrent._
import scala.concurrent.duration._
import model._

@RunWith(classOf[JUnitRunner])
class ShoppingListsServiceSuite extends FunSuite with BeforeAndAfter {
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global
  
  // Class under test
  val service: ShoppingListsService = new ShoppingListsService(MongoTestConnection.db)
  
  before {
  }

  after {
    MongoTestConnection.connection.close()
  }

  test("insert shopping list into database, retrieve it and remove") {
    val list = ShoppingList(name = "christmas-shopping")
    service.save(list)
  
    val future = service.find(list.name)
    future onSuccess { 
      case Some(shoppingList) => assert(shoppingList.name == "christmas-shopping")
      case None => assert(false)
    }
    
    service.remove(list)
  }

}

object MongoTestConnection {
  lazy val driver = new MongoDriver
  lazy val connection = driver.connection(List("localhost"))
  lazy val db = DB("test", connection)
}