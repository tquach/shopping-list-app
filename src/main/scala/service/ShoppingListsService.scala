package service

import reactivemongo.api._
import reactivemongo.bson._
import reactivemongo.api.collections.default.{ BSONCollection }

import play.api.libs.iteratee.Iteratee

import scala.concurrent._
import com.typesafe.scalalogging.slf4j._

import model._

class ShoppingListsService(db: DB) extends Logging {
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  private[this] final val collection = db[BSONCollection]("shopping-lists")

  def find(name: String): Future[Option[ShoppingList]] = {
    logger.debug("Querying for list")
    val query = BSONDocument("$query" -> BSONDocument("name" -> name))
    val cursor = collection.find(query).cursor[ShoppingList]
    cursor.headOption
  }

  def save(shoppingList: ShoppingList) = {
    val insertFuture = collection.insert(shoppingList) 

    insertFuture onSuccess {
      case _ => logger.debug("Saved shopping list")
    }
  }

  def remove(list: ShoppingList) = {
    logger.info("Removing list %s".format(list.name))
    collection.remove(list)
  }
}
