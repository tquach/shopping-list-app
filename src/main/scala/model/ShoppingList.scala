package model

import reactivemongo.bson._

case class ShoppingList(
  id: Option[BSONObjectID] = None,
  name: String)

object ShoppingList {
  implicit object ShoppingListBSONReader extends BSONDocumentReader[ShoppingList] {
    def read(doc: BSONDocument): ShoppingList = 
      ShoppingList(
        doc.getAs[BSONObjectID]("_id"),
        doc.getAs[String]("name").get
      )
  }

  implicit object ShoppingListBSONWriter extends BSONDocumentWriter[ShoppingList] {
    def write(shoppingList: ShoppingList): BSONDocument = BSONDocument(
      "_id" -> shoppingList.id.getOrElse(BSONObjectID.generate),
      "name" -> shoppingList.name
    )
  } 
}