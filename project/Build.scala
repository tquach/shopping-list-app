import sbt._
import Keys._
 
object BuildSettings {
  lazy val buildName = "reactive-mongo-demo"
  lazy val buildOrganization = "ca.walmart"
  lazy val buildScalaVersion = "2.10.2" 
}
 
object Dependencies {
  val junit = "junit" % "junit" % "4.10" % "test"
  val scala_test = "org.scalatest" %% "scalatest" % "1.9.1" % "test"

  val reactive_mongo = "org.reactivemongo" %% "reactivemongo" % "0.9"

  val scalalogging = "com.typesafe" %% "scalalogging-slf4j" % "1.0.1"
}
 
object Application extends Build {
  import BuildSettings._
  import Dependencies._
 
  lazy val root = Project(
    id = buildName,
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := buildName,
      organization := buildOrganization,
      scalaVersion := buildScalaVersion,      
      libraryDependencies ++= Seq(
          scala_test, junit, reactive_mongo, scalalogging
      )
    )
  )
 
}